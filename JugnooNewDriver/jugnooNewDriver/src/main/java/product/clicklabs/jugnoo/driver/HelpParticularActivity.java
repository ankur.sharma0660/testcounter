package product.clicklabs.jugnoo.driver;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import product.clicklabs.jugnoo.driver.datastructure.HelpSection;
import product.clicklabs.jugnoo.driver.retrofit.RestClient;
import product.clicklabs.jugnoo.driver.retrofit.model.BookingHistoryResponse;
import product.clicklabs.jugnoo.driver.utils.ASSL;
import product.clicklabs.jugnoo.driver.utils.AppStatus;
import product.clicklabs.jugnoo.driver.utils.BaseFragmentActivity;
import product.clicklabs.jugnoo.driver.utils.Fonts;
import product.clicklabs.jugnoo.driver.utils.Log;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class HelpParticularActivity extends BaseFragmentActivity {


	LinearLayout relative;

	View backBtn;
	TextView title;
	ProgressBar progressBar;
	TextView textViewInfo;
	WebView webview;


	public static HelpSection helpSection = HelpSection.FARE_DETAILS;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help_particular);

		relative = (LinearLayout) findViewById(R.id.relative);
		new ASSL(HelpParticularActivity.this, relative, 1134, 720, false);


		backBtn = findViewById(R.id.backBtn);
		title = (TextView) findViewById(R.id.title);
		title.setTypeface(Fonts.mavenRegular(getApplicationContext()));

		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		textViewInfo = (TextView) findViewById(R.id.textViewInfo);
		textViewInfo.setTypeface(Fonts.mavenRegular(getApplicationContext()));
		webview = (WebView) findViewById(R.id.webview);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.setWebViewClient(new MyWebViewClient1());
		webview.getSettings().setDomStorageEnabled(true);
		webview.getSettings().setDatabaseEnabled(true);


		//enable Javascript
		webview.getSettings().setJavaScriptEnabled(true);



		if (helpSection != null) {
			title.setText(helpSection.getName(HelpParticularActivity.this));
		}


		backBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				performBackPressed();
			}
		});


		textViewInfo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				getFareDetailsAsync(HelpParticularActivity.this);
			}
		});

		if(helpSection.getOrdinal() == HelpSection.TERMS.getOrdinal()){
			webview.loadUrl("https://uva.uk/legal");
		} else if(helpSection.getOrdinal() == HelpSection.PRIVACY.getOrdinal()){
			webview.loadUrl("https://www.uva.uk/privacy-policy");
		}
		else {
			getFareDetailsAsync(HelpParticularActivity.this);
		}

	}

	boolean loadingFinished = true;
	boolean redirect = false;
	boolean apiCalling = true;

	private class MyWebViewClient1 extends WebViewClient {

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			loadingFinished = false;
			//SHOW LOADING IF IT ISNT ALREADY VISIBLE
			progressBar.setVisibility(View.VISIBLE);
			if(helpSection.getOrdinal()==HelpSection.PRIVACY.getOrdinal())
				progressBar.setVisibility(View.GONE);
			//jugnooAnimation.start();
			Log.e("onPageStarted", "url="+url);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			if(!redirect){
				loadingFinished = true;
			}

//			if(loadingFinished && !redirect && !apiCalling){
//				//HIDE LOADING IT HAS FINISHED
//				progressBar.setVisibility(View.GONE);
//				//jugnooAnimation.stop();
//			} else{
//				redirect = false;
//			}
			progressBar.setVisibility(View.GONE);
			Log.e("onPageFinished", "url="+url);

		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (!loadingFinished) {
				redirect = true;
			}
			loadingFinished = false;
			if (url == null) {
				return false;
			}
			if (url.startsWith("market://")) {
				view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				return true;
			}
			if (url.startsWith("mailto:")) {

				try {
					List<String> to = new ArrayList<String>();
					List<String> cc = new ArrayList<String>();
					List<String> bcc = new ArrayList<String>();
					String subject = null;
					String body = null;

					url = url.replaceFirst("mailto:", "");

					String[] urlSections = url.split("&");
					if (urlSections.length >= 2) {

						to.addAll(Arrays.asList(urlSections[0].split(",")));

						for (int i = 1; i < urlSections.length; i++) {
							String urlSection = urlSections[i];
							String[] keyValue = urlSection.split("=");

							if (keyValue.length == 2) {
								String key = keyValue[0];
								String value = keyValue[1];

								value = URLDecoder.decode(value, "UTF-8");

								if (key.equals("cc")) {
									cc.addAll(Arrays.asList(url.split(",")));
								}
								else if (key.equals("bcc")) {
									bcc.addAll(Arrays.asList(url.split(",")));
								}
								else if (key.equals("subject")) {
									subject = value;
								}
								else if (key.equals("body")) {
									body = value;
								}
							}
						}
					}
					else {
						String[] toArr = url.split(",");
						for(String toI : toArr){
							toI = URLDecoder.decode(toI, "UTF-8");
							to.add(toI);
						}
//                        to.addAll(Arrays.asList(url.split(",")));
					}

					Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
					emailIntent.setType("message/rfc822");

					String[] dummyStringArray = new String[0]; // For list to array conversion
					emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, to.toArray(dummyStringArray));
					if (cc.size() > 0) {
						emailIntent.putExtra(android.content.Intent.EXTRA_CC, cc.toArray(dummyStringArray));
					}
					if (bcc.size() > 0) {
						emailIntent.putExtra(android.content.Intent.EXTRA_BCC, bcc.toArray(dummyStringArray));
					}
					if (subject != null) {
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
					}
					if (body != null) {
						emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
					}
					view.getContext().startActivity(emailIntent);

					return true;
				}
				catch (Exception e) {
					/* Won't happen*/
				}

			}
			return false;
		}
	}


	public void openHelpData(String data, boolean errorOccured) {
		if (errorOccured) {
			textViewInfo.setVisibility(View.VISIBLE);
			textViewInfo.setText(data);
			webview.setVisibility(View.GONE);
		} else {
			textViewInfo.setVisibility(View.GONE);
			webview.setVisibility(View.VISIBLE);
			loadHTMLContent(data);
		}
	}

	public void loadHTMLContent(String data) {
		final String mimeType = "text/html";
		final String encoding = "UTF-8";
		webview.loadDataWithBaseURL("", data, mimeType, encoding, "");
	}



//	Retrofit


	public void getFareDetailsAsync(final Activity activity) {
		if (AppStatus.getInstance(activity).isOnline(activity)) {
			if (helpSection != null) {
				progressBar.setVisibility(View.VISIBLE);
				textViewInfo.setVisibility(View.GONE);
				webview.setVisibility(View.GONE);
				loadHTMLContent("");
				HashMap<String, String> params = new HashMap<>();
				params.put(Constants.KEY_ACCESS_TOKEN, Data.userData != null ? Data.userData.accessToken : JSONParser.getAccessTokenPair(this).first);
				params.put("section", helpSection.getOrdinal()+"");
				HomeUtil.putDefaultParams(params);

				RestClient.getApiServices().getHelpSection(params, new Callback<BookingHistoryResponse>() {


					@Override
					public void success(BookingHistoryResponse bookingHistoryResponse, Response response) {
						try {
							String jsonString = new String(((TypedByteArray) response.getBody()).getBytes());
							JSONObject jObj;
							jObj = new JSONObject(jsonString);
							if (!jObj.isNull("error")) {
								String errorMessage = jObj.getString("error");
								if (Data.INVALID_ACCESS_TOKEN.equalsIgnoreCase(errorMessage.toLowerCase())) {
									HomeActivity.logoutUser(activity, null);
								} else {
									openHelpData(getResources().getString(R.string.error_occured_tap_to_retry), true);
								}
							} else {
								String data = jObj.getString("data");
								openHelpData(data, false);
							}

						} catch (Exception exception) {
							exception.printStackTrace();
							openHelpData(getResources().getString(R.string.error_occured_tap_to_retry), true);
						}
						progressBar.setVisibility(View.GONE);
					}

					@Override
					public void failure(RetrofitError error) {
						progressBar.setVisibility(View.GONE);
						openHelpData(getResources().getString(R.string.error_occured_tap_to_retry), true);

					}
				});
			}
		} else {
			openHelpData(getString(R.string.no_internet), true);
		}
	}


	public void performBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.left_out);
	}

	@Override
	public void onBackPressed() {
		performBackPressed();
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		ASSL.closeActivity(relative);
		System.gc();
	}

}

