package product.clicklabs.jugnoo.driver.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import org.json.JSONObject;

import java.util.HashMap;

import product.clicklabs.jugnoo.driver.Constants;
import product.clicklabs.jugnoo.driver.Data;
import product.clicklabs.jugnoo.driver.HomeActivity;
import product.clicklabs.jugnoo.driver.HomeUtil;
import product.clicklabs.jugnoo.driver.IncomeDetailsActivity;
import product.clicklabs.jugnoo.driver.JSONParser;
import product.clicklabs.jugnoo.driver.R;
import product.clicklabs.jugnoo.driver.datastructure.ApiResponseFlags;
import product.clicklabs.jugnoo.driver.retrofit.RestClient;
import product.clicklabs.jugnoo.driver.retrofit.model.DriverEarningsResponse;
import product.clicklabs.jugnoo.driver.ui.models.FeedCommonResponseKotlin;
import product.clicklabs.jugnoo.driver.utils.AppStatus;
import product.clicklabs.jugnoo.driver.utils.DialogPopup;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WalletCardRechargeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WalletCardRechargeFragment extends Fragment {

    private EditText editTextCode;
    private Activity walletActivity;
    private DriverEarningsFragment driverEarningsFragment;

    public WalletCardRechargeFragment() {
        // Required empty public constructor
    }

    public static WalletCardRechargeFragment newInstance() {
        WalletCardRechargeFragment fragment = new WalletCardRechargeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_wallet_card_recharge, container, false);
        initView(rootView);
        walletActivity = getActivity();
        driverEarningsFragment = new DriverEarningsFragment();
        return rootView;
    }

    private void initView(View rootView) {
        ((TextView) rootView.findViewById(R.id.title)).setText(R.string.wallet_card_recharge);
        rootView.findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                walletActivity.onBackPressed();
            }
        });
        editTextCode = rootView.findViewById(R.id.etCoupon);
        rootView.findViewById(R.id.addMoney).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();
                if ("".equalsIgnoreCase(code)) {
                    DialogPopup.alertPopup(walletActivity, "", getString(R.string.please_enter_coupon_code));
                } else {
                    addMoneyViaWalletCard(code);
                }
                editTextCode.setText("");
            }
        });
    }

    private void addMoneyViaWalletCard(String code) {
        try {
            if (AppStatus.getInstance(walletActivity).isOnline(walletActivity)) {
                HashMap<String, String> map = new HashMap<>();
                map.put(Constants.KEY_ACCESS_TOKEN, Data.userData.accessToken);
                map.put("code", code);
                HomeUtil.putDefaultParams(map);
                RestClient.getApiServices().applyPromo(map, new Callback<FeedCommonResponseKotlin>() {
                    @Override
                    public void success(FeedCommonResponseKotlin feedCommonResponseKotlin, Response response) {
                        try {
                            String jsonString = new String(((TypedByteArray) response.getBody()).getBytes());
                            JSONObject jObj = null;
                            jObj = new JSONObject(jsonString);
                            String message = JSONParser.getServerMessage(jObj);
                            DialogPopup.dialogBanner(walletActivity, message);
                            int flag = jObj.optInt(Constants.KEY_FLAG, ApiResponseFlags.SHOW_MESSAGE.getOrdinal());
                            if(flag == ApiResponseFlags.SHOW_MESSAGE.getOrdinal()){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(getActivity(), IncomeDetailsActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("updateData",1);
                                        startActivity(intent);
                                    }
                                },1200);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

//    private void fetchWalletData() {
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("access_token", Data.userData.accessToken);
//        params.put("login_type", Data.LOGIN_TYPE);
//        params.put("invoice_id", "0");
//        HomeUtil.putDefaultParams(params);
//        RestClient.getApiServices().earningNewDetails(params, new Callback<DriverEarningsResponse>() {
//            @Override
//            public void success(DriverEarningsResponse driverEarningsResponse, Response response) {
//                try {
//                    driverEarningsFragment.updateData(driverEarningsResponse, true);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//    }

  }
