package adapters

import android.app.Activity
import android.graphics.Typeface.BOLD
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.test.R
import kotlinx.android.synthetic.main.list_item_counter.view.*

class CounterAdapter(private val activity: Activity, val list: ArrayList<Int>, val selectedCallback: OnSelectedCallback) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {


    override fun getItemCount(): Int {
        return list.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return ViewHolderVehicle(LayoutInflater.from(activity).inflate(R.layout.list_item_counter, parent, false))
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolderVehicle -> holder.bind(position)
        }


    }

    inner class ViewHolderVehicle(private val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {


        fun bind(position: Int) {
            view.etCounter.setText(list[position].toString())
            view.tvPrint.setOnClickListener(View.OnClickListener { selectedCallback.onPrint(list[position]) })
            view.ivAdd.setOnClickListener(View.OnClickListener {
                list[position]=list[position]+1
                view.etCounter.setText(list[position].toString())
            })
            view.ivMinus.setOnClickListener(View.OnClickListener {
                list[position]=list[position]-1
                view.etCounter.setText(list[position].toString())
            })
        }
    }


    interface OnSelectedCallback{
        fun onPrint(value:Int)
    }
}

