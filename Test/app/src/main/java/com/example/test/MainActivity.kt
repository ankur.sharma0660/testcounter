package com.example.test

import adapters.CounterAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val list =ArrayList<Int>()

        for(x in 0 until 100)
            list.add(0)
        rvCounter.adapter=CounterAdapter(this,list,object : CounterAdapter.OnSelectedCallback{
            override fun onPrint(value: Int) {
                textViewTitle.text= "Count:$value"
            }

        })
        rvCounter.layoutManager = LinearLayoutManager(this)
    }
}